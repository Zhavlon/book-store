import React from 'react';
import BookList from "../bookList";

const MainPage = () => {
	return (
		<div>
			<h1>Main Page</h1>
			<BookList/>
		</div>
	);
};

export default MainPage;